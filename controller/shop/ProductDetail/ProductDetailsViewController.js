import React,{useEffect} from 'react'
import { Text, ScrollView, Image, View, TouchableOpacity } from 'react-native'


import { useSelector, useDispatch } from 'react-redux'
import * as cartAction from '../../../store/actions/cart'

import styles from './style'

const ProductDetailsViewController = props => {

    const disptach = useDispatch()
    const productId = props.route.params.id

    const slectedProduct = useSelector(state => state.produts.avilableProducts.find(product => product.id === productId))

    const didSelectAddToCart = product => {
        disptach(cartAction.addToCart(product))
    }

    useEffect(() => {
        props.navigation.setOptions({
            title: slectedProduct.tittle,
        })
    })

    return (
        <ScrollView>
            <View style={styles.screen}>
                <Image style={styles.imageSetup}
                    source={{ uri: slectedProduct.imageUrl }}
                />

                <TouchableOpacity
                    style={styles.addToCartBkgrd}
                    onPress={() => didSelectAddToCart(slectedProduct)}
                >
                    <View style={styles.addToCartBtn}>
                        <Text>Add to Card</Text>
                    </View>
                </TouchableOpacity>

                <View style={styles.priceView}>
                    <Text>${slectedProduct.price}</Text>
                </View>

                <View>
                    <Text>{slectedProduct.description}</Text>
                </View>

            </View>

        </ScrollView >

    )


}




export default ProductDetailsViewController