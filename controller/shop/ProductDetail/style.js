import { Platform, StyleSheet } from 'react-native'
import Color from '../../../constants/Color'
const styles = StyleSheet.create({
    screen: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        padding: 10
    },
    imageSetup: {
        flex: 0.4,
        width: '90%',
        height: 400,
        borderRadius: 10,
    },
    priceView: {
        margin: 10,
        padding: 10,
        justifyContent: 'center',
        alignItems: 'center'
    },
    detailsView: {
        margin: 10,
        padding: 10,
        justifyContent: 'flex-start',
        alignItems: 'flex-start'
    },
    addToCartBtn:
    {
        justifyContent: 'center',
        alignItems: 'center',
        margin: 10,
        padding: 8
    },
    addToCartBkgrd: {
        backgroundColor: Platform.OS === 'ios' ? Color.iOSHeaderColor : Color.androidHeaderColor,
        borderRadius: 15,
        marginTop: 30
    }
})


export default styles