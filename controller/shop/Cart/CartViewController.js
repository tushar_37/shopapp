import React, { useEffect } from 'react'
import { View, Text, FlatList, TouchableOpacity, Dimensions } from 'react-native'
import styles from './style'
import { useSelector, useDispatch } from 'react-redux'
import CartItem from '../../../components/CartItem/CartItem'
import * as cartAction from '../../../store/actions/cart'
import * as orderAction from '../../../store/actions/order'

const CartViewController = props => {

    const disptach = useDispatch()


    var totalAmount = 0

    const cartItems = useSelector(state => {
        const transformedCartItems = []
        for (const key in state.cart.items) {
            if (key != 'totalAmount') {
                totalAmount += state.cart.items[key].sum
                transformedCartItems.push({
                    productId: key,
                    productTittle: state.cart.items[key].productTittle,
                    productPrice: state.cart.items[key].productPrice,
                    quantity: state.cart.items[key].quantity,
                    sum: state.cart.items[key].sum,
                    imageUrl: state.cart.items[key].imageUrl
                })
            }
        }
        return transformedCartItems

    })


    const setHeaderRight = () => {
        return (
            <TouchableOpacity
                onPress={() => props.navigation.navigate('Your Order')}>
                <Text
                    style={{
                        color: 'white',
                        padding: 10,
                    }}>Orders</Text>
            </TouchableOpacity>
        );
    };

    // By using useEffect we can update the navigation options from any screen
    useEffect(() => {
        props.navigation.setOptions({
            headerRight: setHeaderRight
        })
    })


    return (
        <View style={styles.screen}>
            <View style={styles.cartList}>

                <FlatList
                    data={cartItems}
                    keyExtractor={item => item.productId}
                    renderItem={(itemData) => {
                        return (
                            <CartItem
                                productTittle={itemData.item.productTittle}
                                imageUrl={itemData.item.imageUrl}
                                productPrice={itemData.item.productPrice}
                                productQuantity={itemData.item.quantity}
                                productTotal={itemData.item.sum}
                                onRemove={() => {
                                    disptach(cartAction.removeFromCart(itemData.item.productId))
                                }}
                            // onAdd={() => disptach(cartAction.addToCart(itemData.item))}
                            />
                        )
                    }}
                />
            </View>

            <View style={styles.bottomBar}>

                <View style={styles.priceOverView}>
                    <Text>TOTAL</Text>
                    <Text style={{ fontSize: 24 }}>${totalAmount.toFixed(2)}</Text>
                    <Text>Free Domastic shipping</Text>
                </View>

                <View style={styles.placeOrderContainer}>
                    <TouchableOpacity
                        style={styles.placeOrder}
                        onPress={() => {
                            disptach(orderAction.placeOrder(cartItems, totalAmount))
                        }} >
                        <Text style={{ color: 'white' }}>PLACE ORDER</Text>
                    </TouchableOpacity>
                </View>

            </View>

        </View >
    )
}


export default CartViewController