import { StyleSheet, Platform } from 'react-native'
import Color from '../../../constants/Color'



const styles = StyleSheet.create({
    screen: {
        flex: 1,
    },
    cartList: {
        flex: 0.90,
        backgroundColor: '#f6f6f8',
        justifyContent: 'center',
        alignItems: 'center'
    },
    bottomBar: {
        flex: 0.10,
        flexDirection: 'row',
        backgroundColor: '#ffffff'
    },
    priceOverView: {
        flex: 0.5,
        marginLeft: 20,
        padding: 5
    },
    placeOrderContainer: {
        flex: 0.5,
        justifyContent: 'center',
        alignItems: 'center'
    },
    placeOrder: {
        flexDirection: 'row',
        justifyContent: 'center', alignItems: 'center',
        backgroundColor: Platform.OS === 'ios' ? Color.iOSHeaderColor : Color.androidHeaderColor,
        padding: 10,
        borderRadius: 20
    }
})


export default styles