import React, { useEffect } from 'react'
import { FlatList, View, Text, TouchableOpacity } from 'react-native'
import styles from './style'
import ProductItem from '../../../components/ProductItem/ProductItem'
import { HeaderButtons, Item } from 'react-navigation-header-buttons'
import { useSelector, useDispatch } from 'react-redux'
import CustomHeaderButton from '../../../components/CustomHeaderButton/HeaderButton'
import * as cartAction from '../../../store/actions/cart'


const ProductOverViewController = props => {

    const disptach = useDispatch()

    const products = useSelector(state => state.produts.avilableProducts)


    const setHeaderRight = () => {
        return (
            <TouchableOpacity
                onPress={() => props.navigation.navigate('Cart')}>
                <Text
                    style={{
                        color: 'white',
                        padding: 10,
                    }}>Cart</Text>
            </TouchableOpacity>
        );
    };

    // By using useEffect we can update the navigation options from any screen
    useEffect(() => {
        props.navigation.setOptions({
            title: 'All Products',
            headerRight: setHeaderRight
        })
    })


    const didSelectViewDetails = product => {
        props.navigation.navigate('Details', {
            id: product.id
        })
    }



    return (
        <View style={styles.screen} >
            <FlatList
                data={products}
                keyExtractor={item => item.id}
                renderItem={itemData => {
                    return (
                        <ProductItem
                            tittle={itemData.item.tittle}
                            image={itemData.item.imageUrl}
                            price={itemData.item.price}
                            onViewDetail={() => didSelectViewDetails(itemData.item)}
                            onAddToCart={() => disptach(cartAction.addToCart(itemData.item))}

                        />
                    )
                }} />
        </View>
    )
}



export default ProductOverViewController