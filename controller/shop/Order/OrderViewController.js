import React from 'react'
import { View, Text, FlatList } from 'react-native'
import { useSelector } from 'react-redux'
import OrderItem from '../../../components/OrderItem/OrderItem'
import styles from './style'

const OrderViewController = props => {
    const orders = useSelector(state => state.orders.orders)
    return (
        <View style={styles.screen}>
            <FlatList
                data={orders}
                keyExtractor={item => item.orderId}
                renderItem={
                    itemData => {
                        return (
                            <OrderItem
                                orderId={itemData.item.orderId}
                                date={itemData.item.date}
                                totalAmount={itemData.item.totalAmount}
                                items={itemData.item.items}
                            />
                        )
                    }
                }
            />
        </View>
    )
}


export default OrderViewController