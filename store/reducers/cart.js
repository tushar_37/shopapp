import CartItem from "../../modals/cart-item"
import { ADD_TO_CART, REMOVE_FROM_CART } from "../actions/cart"
import { ADD_ORDER } from "../actions/order"

const initialState = {
    items: {},
    totalAmount: 0
}

export default (state = initialState, action) => {
    switch (action.type) {
        case ADD_TO_CART:
            const addedProduct = action.product
            const price = addedProduct.price
            const tittle = addedProduct.tittle
            const imageUrl = addedProduct.imageUrl
            var totalAmount = state.totalAmount

            let updatedOrNewCartItem;

            if (state.items[addedProduct.id]) {
                totalAmount = state.items[addedProduct.id].sum + price
                updatedOrNewCartItem = new CartItem(
                    state.items[addedProduct.id].quantity + 1,
                    price,
                    tittle,
                    state.items[addedProduct.id].sum + price,
                    imageUrl
                )
            } else {
                totalAmount = price
                updatedOrNewCartItem = new CartItem(1, price, tittle, price, imageUrl)
            }
            console.log('combine total:::', totalAmount)
            return {
                ...state,
                items: { ...state.items, [addedProduct.id]: updatedOrNewCartItem, totalAmount: state.totalAmount + totalAmount }
            }

        case REMOVE_FROM_CART:
            const selectedCartItem = state.items[action.pid];
            const currentQty = selectedCartItem.quantity;
            let updatedCartItems;

            if (currentQty > 1) {
                updatedCartItems = {
                    ...state.items, [action.pid]: new CartItem(
                        selectedCartItem.quantity - 1,
                        selectedCartItem.productPrice,
                        selectedCartItem.productTittle,
                        selectedCartItem.sum - selectedCartItem.productPrice,
                        selectedCartItem.imageUrl
                    )
                };
            } else {

                updatedCartItems = { ...state.items };
                delete updatedCartItems[action.pid];
            }
            return {
                ...state,
                items: updatedCartItems,
                totalAmount: state.totalAmount - selectedCartItem.productPrice
            };

        case ADD_ORDER:
            return initialState

    }
    return state
}


