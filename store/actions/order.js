export const ADD_ORDER = 'ADD_ORDER'

export const placeOrder = (cartItems, totalAmount) => {
    return {
        type: ADD_ORDER,
        orderDetails: {
            finalAmount: totalAmount,
            items: cartItems
        }
    }
}