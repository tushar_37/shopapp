
import React from 'react'
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createDrawerNavigator } from '@react-navigation/drawer'

import { Platform, StyleSheet } from 'react-native'

import ProductOverViewController from '../controller/shop/ProductOverview/ProductOverViewController'

import CartViewController from '../controller/shop/Cart/CartViewController';

import ProductDetailsViewController from '../controller/shop/ProductDetail/ProductDetailsViewController'

import OrderViewController from '../controller/shop/Order/OrderViewController'

import Colors from '../constants/Color'

const Stack = createStackNavigator();

const ProductNavigationStack = () => {
    return (
        <NavigationContainer>
            <Stack.Navigator
                screenOptions={{
                    headerStyle: styles.header,
                    headerTintColor: 'white',
                }}>
                <Stack.Screen
                    name="Products"
                    component={ProductOverViewController} />
                <Stack.Screen
                    name="Details"
                    component={ProductDetailsViewController} />
                <Stack.Screen
                    name="Cart"
                    component={CartViewController} />
                <Stack.Screen
                    name="Your Order"
                    component={OrderViewController} />
            </Stack.Navigator>
        </NavigationContainer>
    )
}


const OrdersNavigationStack = () => {
    <NavigationContainer>
        <Stack.Navigator
            screenOptions={{
                headerStyle: styles.header,
                headerTintColor: 'white',
            }}>
            <Stack.Screen
                name="Orders"
                component={OrderViewController} />
        </Stack.Navigator>
    </NavigationContainer>
}


const Drawer = createDrawerNavigator()

const ShopNavigator = () => {
    return (
        <Drawer.Navigator>
            <Drawer.Screen name="Orders" component={OrdersNavigationStack} />
        </Drawer.Navigator>
    )
}

const styles = StyleSheet.create({
    header: {
        backgroundColor: Platform.OS === 'ios' ? Colors.iOSHeaderColor : Colors.androidHeaderColor
    }
})

export default ProductNavigationStack




//yarn add @react-navigation/stack