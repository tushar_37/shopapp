class CartItem {
    constructor(quantity, productPrice, productTittle, sum, imageUrl) {
        this.quantity = quantity
        this.productPrice = productPrice
        this.productTittle = productTittle
        this.sum = sum
        this.imageUrl = imageUrl
    }
}

export default CartItem