import { StyleSheet, Dimensions, Platform } from 'react-native'


const styles = StyleSheet.create({
    mainView: {
        flexDirection: 'row',
        height: 150,
        width: Dimensions.get('window').width - 20,
        borderRadius: 10
    },
    leftSection: {
        flex: 0.40,
        justifyContent: 'center',
        alignItems: 'center'
    },
    rightSection: {
        flex: 0.60,
        margin: 10,
        padding: 10,
    },
    imageSetup: {
        borderRadius: 50,
        backgroundColor: '#ffffff',
        width: 100,
        height: 100,
    },
    tittle: {
        fontSize: 28,
        color: 'red'
    },
    price: {
        fontSize: 16,
        color: 'black'
    },
    quantity: {
        fontSize: 24,
        color: 'red',
        borderRadius: 10,
        borderWidth: 1,
        padding: 5
    },
    sum: {
        fontSize: 22,
        color: 'grey'
    },
    manageQuantity: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        margin: 5
    },
    buttonSetup: {
        backgroundColor: 'white',
        width: 40,
        height: 40,
        borderRadius: 20,
        justifyContent: 'center',
        alignItems: 'center',
        shadowColor: 'black',
        shadowRadius: 10
    },
    buttonText:{ color: 'red', fontSize: 26 }
})


export default styles