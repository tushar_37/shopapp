
import React from 'react'
import { View, Image, Text, TouchableOpacity } from 'react-native'
import styles from './style'


const CartItem = ({ imageUrl, productTittle, productPrice, productTotal, productQuantity, onRemove, onAdd, isHideManageQuantity = false }) => {
    return (
        <View style={styles.mainView}>

            <View style={styles.leftSection}>

                <Image style={styles.imageSetup}
                    source={{ uri: imageUrl }}
                />

            </View>

            <View style={styles.rightSection} >

                <Text style={styles.tittle}>{productTittle}</Text>

                <Text style={styles.price}>price: ${productPrice}</Text>

                {!isHideManageQuantity && <View style={styles.manageQuantity}>

                    <TouchableOpacity
                        onPress={onRemove}
                        style={styles.buttonSetup}>
                        <Text style={styles.buttonText}>-</Text>
                    </TouchableOpacity>

                    <Text style={styles.quantity}>{productQuantity}</Text>

                    <TouchableOpacity
                        onPress={onAdd}
                        style={styles.buttonSetup}>
                        <Text style={styles.buttonText}>+</Text>
                    </TouchableOpacity>

                </View>}

                <Text style={styles.sum}>Total: {productTotal}</Text>

            </View>
        </View >
    )
}


export default CartItem