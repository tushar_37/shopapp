import React from 'react'
import { View, Text, Button, Image, TouchableOpacity, Platform } from 'react-native'
import styles from './style'


const ProductItem = ({ tittle, price, image, onViewDetail, onAddToCart }) => {
    return (
        <View style={styles.product}>
            <View style={styles.imageContainer}>
                <Image style={styles.image} source={{ uri: image }} />
            </View>
            <View style={styles.details}>
                <Text style={styles.title}>{tittle}</Text>
                <Text style={styles.price}>${price.toFixed(2)}</Text>
            </View>
            <View style={styles.actions}>
                <TouchableOpacity
                    style={styles.actionView}
                    onPress={onViewDetail}
                >
                    <View>
                        <Text style={{ color: 'white' }}>View Details</Text>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity 
                style={styles.actionView}
                onPress={onAddToCart}>
                    <View>
                        <Text style={{ color: 'white' }}>Add to Cart</Text>
                    </View>
                </TouchableOpacity>
            </View>
        </View>
    )
}



export default ProductItem