
import { StyleSheet, Dimensions, Platform } from 'react-native'
import Colors from '../../constants/Color'

const styles = StyleSheet.create({
    product: {
        shadowColor: 'black',
        shadowOpacity: 0.26,
        shadowOffset: { width: 0, height: 2 },
        shadowRadius: 8,
        elevation: 5,
        borderRadius: 10,
        backgroundColor: 'white',
        height: Dimensions.get('window').width * 0.70,
        width: Dimensions.get('window').width * 0.95,
        margin: 10,
    },
    imageContainer: {
        width: '100%',
        height: '60%',
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        overflow: 'hidden'
    },
    image: {
        width: '100%',
        height: '100%'
    },
    details: {
        alignItems: 'center',
        height: '15%',
        padding: 10
    },
    title: {
        fontSize: 18,
        marginVertical: 4
    },
    price: {
        fontSize: 14,
        color: '#888'
    },
    actions: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        height: '25%',
        padding: 10
    },
    actionView: {
        padding: 5,
        backgroundColor: Platform.OS === 'ios' ? Colors.iOSHeaderColor : Colors.androidHeaderColor,
        borderRadius: 8,
        height: 40,
        justifyContent: 'center',
        alignItems: 'center'
    }
})

export default styles