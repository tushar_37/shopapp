import { Dimensions, Platform, StyleSheet } from 'react-native'
import Color from '../../constants/Color'

const styles = StyleSheet.create({
    orderItem: {
        width: Dimensions.get('window').width - 20,
        backgroundColor: 'white',
        borderRadius: 10,
        margin: 10,
        padding: 10,
        justifyContent: 'center',
        alignItems: 'center'
    },
    infoTittle: { color: 'black', fontSize: 16 },
    infoValue: { color: 'grey', fontSize: 24 },
    orderIdSummery: {
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 10,
        backgroundColor: '#f7fbfd',
        padding: 10
    },
    orderDetailsSummery: {
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 10,
        backgroundColor: Platform.OS === 'ios' ? Color.iOSHeaderColor : Color.androidHeaderColor,
        padding: 10,
        margin: 5,
        width: 150
    }
})

export default styles