import React, { useState } from 'react'
import { View, Text, TouchableOpacity } from 'react-native'
import CartItem from '../CartItem/CartItem'
import styles from './style'

const OrderItem = ({ orderId, totalAmount, items }) => {

    const [showDetails, setShowDetails] = useState(false)
    return (
        <View style={styles.orderItem}>
            <View
                style={styles.orderIdSummery}>
                <Text
                    style={styles.infoTittle}>Order Id</Text>
                <Text
                    numberOfLines={3}
                    style={styles.infoValue}>{orderId}</Text>

                <Text
                    style={styles.infoTittle}>Total Amount</Text>
                <Text
                    style={styles.infoValue}>{totalAmount.toFixed(2)}</Text>

            </View>

            <TouchableOpacity
                style={styles.orderDetailsSummery}
                onPress={() => { setShowDetails(preState => !preState) }}
            >
                <Text
                    style={{ color: 'white' }}>Show Details</Text>
            </TouchableOpacity >

            {showDetails && <View>
                {
                    items.map(cartItem => {
                        return (
                            <CartItem
                                productTittle={cartItem.productTittle}
                                imageUrl={cartItem.imageUrl}
                                productPrice={cartItem.productPrice.toFixed(2)}
                                productQuantity={cartItem.quantity}
                                productTotal={cartItem.sum.toFixed(2)}
                                isHideManageQuantity={true}
                            />
                        )
                    })
                }
            </View>}
        </View >
    )
}


export default OrderItem