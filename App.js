import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

import { createStore, combineReducers } from 'redux'
import { Provider } from 'react-redux'
import productReducers from './store/reducers/products'
import cartReducers from './store/reducers/cart'
import ordersReducers from './store/reducers/order'

import ProductNavigationStack from './navigation/ShopNavigation'

const rootReducers = combineReducers({
  produts: productReducers,
  cart: cartReducers,
  orders: ordersReducers
})

const store = createStore(rootReducers)


export default function App() {
  return (
    <Provider
      store={store}>
      <ProductNavigationStack />
    </Provider>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
